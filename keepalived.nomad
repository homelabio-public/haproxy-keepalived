locals {
  config = jsondecode(file("runnervars.json"))
}

job "keepalived" {
  region      = "global"
  datacenters = ["home"]
  type        = "service"

  namespace = "infrastructure"

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "arm64"
  }

  reschedule {
    delay          = "2m"
    delay_function = "constant"
    unlimited      = true
  }

  spread {
    attribute = "${node.unique.id}"
  }

  update {
    max_parallel      = 1
    health_check      = "checks"
    min_healthy_time  = "10s"
    healthy_deadline  = "5m"
    progress_deadline = "10m"
    auto_revert       = true
    canary            = 0
    stagger           = "30s"
  }

  group "master" {
    network {
      mode = "bridge"

      port "https" {
        static       = "7443"
        host_network = "vlan100"
      }
    }

    constraint {
      attribute = "${attr.unique.consul.name}"
      value     = "rpi0"
    }

    service {
      port = "https"
      name = "proxy-external"
      tags = ["rpi0"]
      task = "proxy-external"
      address_mode = "alloc"

      check {
        name     = "healthcheck"
        type     = "script"
        task     = "proxy-external"
        command  = "/usr/bin/wget"
        args     = ["-O", "-", "http://localhost:8080/stats"]
        interval = "10s"
        timeout  = "60s"
      }

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "qbit"
              local_bind_port = 3001
            }
            upstreams {
              destination_name = "gitlab"
              local_bind_port = 3005
            }
            upstreams {
              destination_name = "gitlab-registry"
              local_bind_port = 3006
            }
            upstreams {
              destination_name = "grafana"
              local_bind_port = 3007
            }
          }
        }
      }
    }

    task "proxy-external" {
      driver = "docker"

      vault {
        policies = ["kv-v2","nomad-cluster-tls-policy"]
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }

      config {
        image = "${local.config.PROXY_EXTERNAL_IMAGE_NAME}:${local.config.PROXY_EXTERNAL_IMAGE_VERSION}"

        volumes = [
          "local/haproxy.cfg:/usr/local/etc/haproxy/haproxy.cfg"
        ]
      }

      resources {
        cpu    = 300
        memory = 300
      }

      template {
        data          = file("conf/local_ca.pem.tpl")
        destination   = "${NOMAD_TASK_DIR}/cacerts/local_ca.pem"
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }

      template {
        data          = file("conf/local_fullchain.pem.tpl")
        destination   = "${NOMAD_SECRETS_DIR}/certs/local_fullchain.pem"
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }

      template {
        data          = file("conf/certbot_fullchain.pem.tpl")
        destination   = "${NOMAD_SECRETS_DIR}/certs/certbot_fullchain.pem"
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }

      template {
        data = file("conf/haproxy.cfg")
        destination = "local/haproxy.cfg"
      }
    }

    task "keepalive-master" {
      driver = "docker"

      lifecycle {
        hook    = "poststart"
        sidecar = true
      }

      vault {
        policies      = ["kv-v2"]
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }

      config {
        image        = "${local.config.KEEPALIVE_IMAGE_NAME}:${local.config.KEEPALIVE_IMAGE_VERSION}"
        cap_add      = ["NET_ADMIN", "NET_BROADCAST", "NET_RAW"]
        network_mode = "host"
      }

      env {
        KEEPALIVED_COMMAND_LINE_ARGUMENTS = "--use-file=${NOMAD_SECRETS_DIR}/keepalived.conf"
      }

      resources {
        memory = 300
      }

//       template {
//         data = <<EOH
// KEEPALIVED_INTERFACE = "vlan100"
// KEEPALIVED_OPTIONS = "--log-detail"
// KEEPALIVED_PASSWORD = "{{ with secret "kv-v2/infrastructure/keepalived" }}{{ .Data.data.password }}{{ end }}"
// KEEPALIVED_PRIORITY = "150"
// KEEPALIVED_ROUTER_ID = "52"
// KEEPALIVED_UNICAST_PEERS = #PYTHON2BASH:['rpi0.proxy-external.service.consul', 'rpi2.proxy-external.service.consul']
// KEEPALIVED_VIRTUAL_IPS = "10.0.7.2"
// KEEPALIVED_STATE = "BACKUP"
// EOH
//         destination = "secrets/file.env"
//         env = true
//       }

      template {
        // data          = file("conf/keepalived.conf.tpl")
        data          = file("conf/proxy-a.conf.tpl")
        destination   = "${NOMAD_SECRETS_DIR}/keepalived.conf"
        change_mode   = "signal"
        change_signal = "SIGTERM"
      }

      template {
        data          = file("conf/notify.sh")
        destination   = "${NOMAD_SECRETS_DIR}/notify.sh"
        perms         = "755"
        change_mode   = "signal"
        change_signal = "SIGTERM"
      }
    }
  }

  group "backup" {
    network {
      mode = "bridge"

      port "https" {
        static       = "7443"
        host_network = "vlan100"
      }
    }

    constraint {
      attribute = "${attr.unique.consul.name}"
      value     = "rpi2"
    }

    service {
      port = "https"
      name = "proxy-external"
      tags = ["rpi2"]
      task = "proxy-external"
      address_mode = "alloc"

      check {
        name     = "healthcheck"
        type     = "script"
        task     = "proxy-external"
        command  = "/usr/bin/wget"
        args     = ["-O", "-", "http://localhost:8080/stats"]
        interval = "10s"
        timeout  = "60s"
      }

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "qbit"
              local_bind_port = 3001
            }
            upstreams {
              destination_name = "gitlab"
              local_bind_port = 3005
            }
            upstreams {
              destination_name = "gitlab-registry"
              local_bind_port = 3006
            }
            upstreams {
              destination_name = "grafana"
              local_bind_port = 3007
            }
          }
        }
      }
    }

    task "proxy-external" {
      driver = "docker"

      vault {
        policies      = ["kv-v2","nomad-cluster-tls-policy"]
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }

      config {
        image   = "${local.config.PROXY_EXTERNAL_IMAGE_NAME}:${local.config.PROXY_EXTERNAL_IMAGE_VERSION}"
        volumes = [
          "local/haproxy.cfg:/usr/local/etc/haproxy/haproxy.cfg"
        ]
      }

      resources {
        cpu    = 300
        memory = 300
      }

      template {
        data          = file("conf/local_ca.pem.tpl")
        destination   = "${NOMAD_TASK_DIR}/cacerts/local_ca.pem"
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }

      template {
        data          = file("conf/local_fullchain.pem.tpl")
        destination   = "${NOMAD_SECRETS_DIR}/certs/local_fullchain.pem"
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }

      template {
        data          = file("conf/certbot_fullchain.pem.tpl")
        destination   = "${NOMAD_SECRETS_DIR}/certs/certbot_fullchain.pem"
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }

      template {
        data        = file("conf/haproxy.cfg")
        destination = "local/haproxy.cfg"
      }
    }
 
    task "keepalive-backup" {
      driver = "docker"

      lifecycle {
        hook    = "poststart"
        sidecar = true
      }

      vault {
        policies      = ["kv-v2"]
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }

      config {
        image        = "${local.config.KEEPALIVE_IMAGE_NAME}:${local.config.KEEPALIVE_IMAGE_VERSION}"
        cap_add      = ["NET_ADMIN", "NET_BROADCAST", "NET_RAW"]
        network_mode = "host"
      }

      env {
        KEEPALIVED_COMMAND_LINE_ARGUMENTS = "--use-file=${NOMAD_SECRETS_DIR}/keepalived.conf"
      }

      resources {
        memory = 300
      }

//       template {
//         data = <<EOH
// KEEPALIVED_INTERFACE = "vlan100"
// KEEPALIVED_OPTIONS = "--log-detail"
// KEEPALIVED_PASSWORD = "{{ with secret "kv-v2/infrastructure/keepalived" }}{{ .Data.data.password }}{{ end }}"
// KEEPALIVED_PRIORITY = "150"
// KEEPALIVED_ROUTER_ID = "52"
// KEEPALIVED_UNICAST_PEERS = #PYTHON2BASH:['rpi0.proxy-external.service.consul', 'rpi2.proxy-external.service.consul']
// KEEPALIVED_VIRTUAL_IPS = "10.0.7.2"
// KEEPALIVED_STATE = "BACKUP"
// EOH
//         destination = "secrets/file.env"
//         env = true
//       }

      template {
        // data          = file("conf/keepalived.conf.tpl")
        data          = file("conf/proxy-b.conf.tpl")
        destination   = "${NOMAD_SECRETS_DIR}/keepalived.conf"
        perms         = "755"
        change_mode   = "signal"
        change_signal = "SIGTERM"
      }

      template {
        data          = file("conf/notify.sh")
        destination   = "${NOMAD_SECRETS_DIR}/notify.sh"
        perms         = "755"
        change_mode   = "signal"
        change_signal = "SIGTERM"
      }
    }
  }
}
