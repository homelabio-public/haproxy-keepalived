locals {
  config = jsondecode(file("runnervars.json"))
}

job "haproxy" {
  region      = "global"
  datacenters = ["home"]
  type        = "service"

  namespace = "infrastructure"

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "arm64"
  }

  reschedule {
    delay          = "2m"
    delay_function = "constant"
    unlimited      = true
  }

  spread {
    attribute = "${node.unique.id}"
  }

  update {
    max_parallel      = 1
    health_check      = "checks"
    min_healthy_time  = "10s"
    healthy_deadline  = "5m"
    progress_deadline = "10m"
    auto_revert       = true
    canary            = 1
    stagger           = "30s"
  }

  group "primary" {
    network {
      mode = "bridge"

      port "https" {
        host_network = "vlan100"
        static       = "443"
      }
    }

    constraint {
      attribute = "${attr.unique.consul.name}"
      value     = "rpi0"
    }

    service {
      port = "https"
      name = "proxy-external"
      tags = ["rpi0"]
      task = "proxy-external"
      address_mode = "alloc"

      check {
        name     = "healthcheck"
        type     = "script"
        task     = "proxy-external"
        command  = "/usr/bin/wget"
        args     = ["-O", "-", "http://localhost:8080/stats"]
        interval = "10s"
        timeout  = "60s"
      }

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "qbit"
              local_bind_port = 3001
            }
            upstreams {
              destination_name = "gitlab"
              local_bind_port = 3005
            }
            upstreams {
              destination_name = "gitlab-registry"
              local_bind_port = 3006
            }
            upstreams {
              destination_name = "grafana"
              local_bind_port = 3007
            }
          }
        }
      }
    }

    task "proxy-external" {
      driver = "docker"

      vault {
        policies = ["kv-v2","nomad-cluster-tls-policy"]
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }

      config {
        image = "${local.config.PROXY_EXTERNAL_IMAGE_NAME}:${local.config.PROXY_EXTERNAL_IMAGE_VERSION}"

        volumes = [
          "local/haproxy.cfg:/usr/local/etc/haproxy/haproxy.cfg"
        ]
      }

      resources {
        cpu    = 300
        memory = 300
      }

      template {
        data          = file("conf/local_ca.pem.tpl")
        destination   = "${NOMAD_TASK_DIR}/cacerts/local_ca.pem"
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }

      template {
        data          = file("conf/local_fullchain.pem.tpl")
        destination   = "${NOMAD_SECRETS_DIR}/certs/local_fullchain.pem"
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }

      template {
        data          = file("conf/certbot_fullchain.pem.tpl")
        destination   = "${NOMAD_SECRETS_DIR}/certs/certbot_fullchain.pem"
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }

      template {
        data = file("conf/haproxy.cfg")
        destination = "local/haproxy.cfg"
      }
    }
  }

  group "secondary" {
    network {
      mode = "bridge"

      port "https" {
        host_network = "vlan100"
        static       = "443"
      }
    }

    constraint {
      attribute = "${attr.unique.consul.name}"
      value     = "rpi2"
    }

    service {
      port = "https"
      name = "proxy-external"
      tags = ["rpi2"]
      task = "proxy-external"
      address_mode = "alloc"

      check {
        name     = "healthcheck"
        type     = "script"
        task     = "proxy-external"
        command  = "/usr/bin/wget"
        args     = ["-O", "-", "http://localhost:8080/stats"]
        interval = "10s"
        timeout  = "60s"
      }

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "qbit"
              local_bind_port = 3001
            }
            upstreams {
              destination_name = "gitlab"
              local_bind_port = 3005
            }
            upstreams {
              destination_name = "gitlab-registry"
              local_bind_port = 3006
            }
            upstreams {
              destination_name = "grafana"
              local_bind_port = 3007
            }
          }
        }
      }
    }

    task "proxy-external" {
      driver = "docker"

      vault {
        policies      = ["kv-v2","nomad-cluster-tls-policy"]
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }

      config {
        image   = "${local.config.PROXY_EXTERNAL_IMAGE_NAME}:${local.config.PROXY_EXTERNAL_IMAGE_VERSION}"
        volumes = [
          "local/haproxy.cfg:/usr/local/etc/haproxy/haproxy.cfg"
        ]
      }

      resources {
        cpu    = 300
        memory = 300
      }

      template {
        data          = file("conf/local_ca.pem.tpl")
        destination   = "${NOMAD_TASK_DIR}/cacerts/local_ca.pem"
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }

      template {
        data          = file("conf/local_fullchain.pem.tpl")
        destination   = "${NOMAD_SECRETS_DIR}/certs/local_fullchain.pem"
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }

      template {
        data          = file("conf/certbot_fullchain.pem.tpl")
        destination   = "${NOMAD_SECRETS_DIR}/certs/certbot_fullchain.pem"
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }

      template {
        data        = file("conf/haproxy.cfg")
        destination = "local/haproxy.cfg"
      }
    }
  }
}
