{{ with secret "kv-v2/letsencrypt/certificates/live/novuscotia.com" }}
{{ .Data.data.fullchain }}
{{ .Data.data.privkey }}{{ end }}