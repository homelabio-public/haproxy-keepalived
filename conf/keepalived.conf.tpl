global_defs {
  default_interface vlan100
  router_id {{ env "attr.unique.consul.name" }} # The hostname of this host
}

vrrp_instance VI_1 {
  interface vlan100

  state BACKUP
  virtual_router_id 52
  priority 100
  nopreempt

  unicast_peer {
    10.0.7.15
    10.0.7.17
  }

  virtual_ipaddress {
    10.0.7.2
  }

  authentication {
    auth_type PASS
    auth_pass {{ with secret "kv-v2/infrastructure/keepalived" }}{{ .Data.data.password }}{{ end }}
  }

  notify "/container/service/keepalived/assets/notify.sh"
}

virtual_server 10.0.7.2 443 {
    delay_loop 5
    lb_algo wlc
    lb_kind NAT
    persistence_timeout 600
    protocol TCP

    real_server 10.0.7.15 7443 {
        weight 1
        TCP_CHECK {
          connect_timeout 10
          connect_port    8080
        }
    }

    real_server 10.0.7.17 7443 {
        weight 1
        TCP_CHECK {
          connect_timeout 10
          connect_port    8080
        }
    }
}
