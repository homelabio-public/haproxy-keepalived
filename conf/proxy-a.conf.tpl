global_defs {
  router_id {{ env "attr.unique.consul.name" }} # The hostname of this host
  enable_script_security
  script_user nobody
  max_auto_priority 99
}
   
vrrp_script chk_haproxy {
  script "/usr/bin/curl http://rpi0.proxy-external.service.consul:8080/stats"
  interval 2
  weight 2
  rise 3
  fall 3
}
   
vrrp_instance haproxy-vip {
  state MASTER
  priority 150
  interface vlan100              # Network card
  virtual_router_id 60
  advert_int 1
  authentication {
    auth_type PASS
    auth_pass {{ with secret "kv-v2/infrastructure/keepalived" }}{{ .Data.data.password }}{{ end }}
  }
  unicast_src_ip 10.0.7.15/24    # The IP address of this machine
  unicast_peer {
    10.0.7.17                    # The IP address of peer machines
  }
   
  virtual_ipaddress {
    10.0.7.2/24                  # The VIP address
  }
   
  track_script {
    chk_haproxy
  }

  notify "/local/notify.sh"
}