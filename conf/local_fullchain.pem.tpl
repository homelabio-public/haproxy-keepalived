{{$host := env "attr.unique.hostname"}}
{{$hostStr := printf "alt_names=%s.node.home.consul,%s.proxy-external.service.consul,localhost" $host}}
{{$ip := env "attr.unique.network.ip-address"}}
{{$ipStr := printf "ip_sans=127.0.0.1,%s" $ip}}

{{ with secret "pki_int/issue/nomad-cluster" "common_name=proxy-external.service.consul" "ttl=72h" $ipStr $hostStr }}
{{ .Data.certificate }}
{{ .Data.issuing_ca }}
{{ .Data.private_key }}{{ end }}